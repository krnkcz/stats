namespace StatsApp

open System

module Config = 
    type ParamName = 
      | AppPort
      | AppHost
      | DbHost
      | DbPort
      | DbName
      | DbUser
      | DbPass

    let private getVarName (param : ParamName) = match param with | AppPort -> "STATS_APP_PORT"
                                                                  | AppHost -> "STATS_APP_HOST"  
                                                                  | DbHost -> "DB_HOST"
                                                                  | DbPort -> "DB_PORT"
                                                                  | DbName -> "DB_NAME"
                                                                  | DbUser -> "DB_USER"
                                                                  | DbPass -> "DB_PASS"

    let getVar (param : ParamName) = getVarName param |> Environment.GetEnvironmentVariable


     // Helper functions 
    let port = getVar AppPort |> int
    let host = getVar AppHost

    let dbHost = getVar DbHost
    let dbPort = getVar DbPort |> int
    let dbName = getVar DbName
    let dbUser = getVar DbUser
    let dbPass = getVar DbPass

    let private connectionPattern = sprintf "Host=%s;Port=%d;Database=%s;Username=%s;Timeout=200;Password=%s" 

    let dbConnection = connectionPattern dbHost dbPort dbName dbUser dbPass


