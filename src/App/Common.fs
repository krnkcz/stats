namespace StatsApp

module Common =
    let toString obj = 
        sprintf "%A" obj   

    let tee f obj = 
        f obj
        obj 

    let teePrint obj =
        tee (printfn "%A") obj 
        
    type Result<'TSuccess,'TFailure> = 
        | Success of 'TSuccess
        | Fail of 'TFailure
    
    let bind switchFunction twoTrackInput = 
        match twoTrackInput with
         | Success s -> switchFunction s
         | Fail f -> Fail f

    let (>=>) switch1 switch2 x = 
        match switch1 x with
        | Success s -> switch2 s
        | Fail f -> Fail f     

    let switch f x = f x |> Success

    let map oneTrackFunction twoTrackInput = 
        match twoTrackInput with
        | Success s -> Success (oneTrackFunction s)
        | Fail f -> Fail f
