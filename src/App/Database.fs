namespace StatsApp

open Common
open Serilog 
open Npgsql
open Dapper
open System.Threading


module Sql = 
   let private retryLimit = 5 
   let private waitingTime = 5
   let private connection = new NpgsqlConnection(Config.dbConnection)

   type SqlQuery =
    {
        Query : string
        Parameters : (string * obj) list
    }

   let p name value = (name,value)

   let sql query parameters =
    {
        Query = query
        Parameters = parameters
    }

   let rec private retryableOperation<'a> = fun  attempt (op : SqlQuery -> 'a) query ->
    Log.Debug ("Trying to connect to DB.") 
    try
        op query |> Success
    with | ex -> 
          Log.Debug ("Unable to connect to DB. Will try again in {time} second(s).",waitingTime)  
          match attempt with | 0 -> Fail (sprintf "Unable to connect to Database, because of: %s" ex.Message)
                             | _ -> Thread.Sleep (1000*waitingTime); retryableOperation (attempt-1) op query 


   let retryableQuery<'t> = retryableOperation retryLimit (fun q -> connection.Query<'t> (q.Query,dict q.Parameters)) 

   let retryableCommand = retryableOperation retryLimit (fun q -> connection.Execute (q.Query,dict q.Parameters))

   let mapInsertResult result = match result with | Success int -> Success (sprintf "Inserted %d row(s)." int)
                                                  | Fail msg -> Fail msg  
module Repository = 
    open Sql

    let getSchemaVersion = 
        let singleRow result = let optionResult = Seq.tryHead result
                                 in match optionResult with | Some res -> Success res
                                                            | None -> Fail "Unable to get schema version from from DB." 
                                     in 
                                      let singleRow2  = retryableQuery<int> >=> singleRow
                                        in sql "select schema_version from public.system;" [] |> singleRow2


    let insert timestamp refferer url = sql """insert into public.signal (timestamp,referrer,page) values(@Timestamp,@Refferer,@Url);""" 
                                          [p "Timestamp" timestamp 
                                           p "Refferer" refferer 
                                           p "Url" url]    
                                                                      
    let insertSignal timestamp referrer url = retryableCommand (insert timestamp referrer url) |> map (sprintf "Inserted %d row(s).")                           
                                            

