﻿namespace StatsApp

open Suave
open Suave.Successful
open Common
open Suave.Filters
open Suave.Operators
open Suave.Writers
open Suave.SerilogExtensions
open Serilog 

module App = 

  let cfg =  
    { defaultConfig with
        bindings =
          [ HttpBinding.createSimple HTTP Config.host Config.port ] 
     }

  // Global
  Log.Logger <-
        LoggerConfiguration()
          .Destructure.FSharpTypes() 
          .MinimumLevel.Debug()
          .WriteTo.Console()
          .CreateLogger()


  let logSchemaVersion = 
    let version = Repository.getSchemaVersion
      in match version with Success v -> Log.Information ("Connected to DB. Schema version: {schema}", v)
                            | Fail message -> Log.Error("Unable to get schema version from DB. Error: {message}", message)

  let handleRequest (req : HttpRequest) =
    let getString (rawForm : byte[]) : string = System.Text.Encoding.UTF8.GetString(rawForm)   
    req.rawForm |>  
    getString |> 
    Api.postSignal |> sprintf "Request result: %A"
  

  let setCORSHeaders =
        addHeader  "Access-Control-Allow-Origin" "*"
        >=> addHeader "Access-Control-Allow-Headers" "content-type"
    
  let webAppWithLogging webApp = SerilogAdapter.Enable(webApp)

// TODO: move script to more  convinient place (CDN)
  let script = "!function(){let e={signal:{url:window.location.href,referer:document.referrer}};fetch(\"https:\/\/stats.kornakiewi.cz\/api\/\",{method:\"POST\",mode:\"cors\",body:JSON.stringify(e)}).then(e=>{console.log(\"Request complete! response:\",e)})}();"

  let statusHandler = choose [path "/status/";path "/"] >=> OK("Working.")
  let scripthandler = path "/script.js" >=> OK(script)
  let apiHandler =  path "/api/" >=> request (handleRequest >>  OK) >=> setCORSHeaders 


  let app = choose[ 
                  GET >=> choose[ 
                        statusHandler
                        scripthandler
                  ] 
                  POST >=> apiHandler
                 ]


  [<EntryPoint>]
  let main argv =
    Log.Information "Starting up..."
    logSchemaVersion
    startWebServer cfg (webAppWithLogging app)
    Log.CloseAndFlush()
    0 // return an integer exit 
    
    
