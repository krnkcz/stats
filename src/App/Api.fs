namespace StatsApp
open Serilog
    module Api = 
        open Common

        let logRequest (request : string) = Log.Information("Received request: {Request}", request)
        let logResult (result : Result<string,string>) = match result with | Success msg -> Log.Debug("Operation finished: {Message}", msg)
                                                                           | Fail msg -> Log.Error("Operation finished: {Message}", msg)

        let postSignal (request : string) = 
            let persist (signal : Signal.Signal) = Repository.insertSignal signal.TimeStamp (Option.defaultValue "" signal.Referrer) signal.Url
            request |>    
            tee logRequest |> 
            (Signal.create >=> persist) |>
            tee logResult

          