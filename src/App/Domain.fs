namespace StatsApp

open FSharp.Data
open System
open Common

module Json = 
    type Signal = JsonProvider<"./jsons/signal.json", InferTypesFromValues=false>
    let parseSignal = Signal.Parse

module Signal =
    type SignalDto = {Url : string; Referrer : string; TimeStamp : DateTime}

    let fromJsonString jsonString = 
      try    
        let parsedJson = Json.parseSignal jsonString
        let url = parsedJson.Signal.Url
        let referer = parsedJson.Signal.Referer
        let timeStamp = DateTime.Now    
        in 
          Success {Url = url; Referrer = referer; TimeStamp = timeStamp}
       with Failure(msg) -> Fail ("Unable to parse Json " + msg)
 

    type Url = string    
    type Referrer = string    

    type Signal = {Url : string; TimeStamp : DateTime ; Referrer : Referrer Option}


    let validateUrl (dto : SignalDto) = match dto.Url with | "" ->  Fail "Missing mandatory URL."
                                                           | _ -> Success dto 


    let validateDto = validateUrl

    let copyDto (dto : SignalDto) = 
        let referer = match dto.Referrer with | "" -> None
                                              | _ -> Some dto.Referrer 
        in {
            Url = dto.Url
            TimeStamp = dto.TimeStamp
            Referrer = referer
        }
    
    let createFromDto  = validateDto >> map copyDto
    
    let create = fromJsonString >=> createFromDto


        