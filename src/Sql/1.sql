-- One table for signals

CREATE TABLE public.system (
	"schema_version" int NOT NULL
);

INSERT INTO public.system VALUES(1);

CREATE TABLE public.signal (
	id serial NOT NULL,
	"timestamp" timestamptz NOT NULL,
	referrer varchar(200) NULL,
	page varchar(300) NOT NULL,
	CONSTRAINT signal_pk PRIMARY KEY (id)
);
