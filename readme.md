##### Tl;dr:
Personal/learning project to create a privacy-oriented web statistics in F#. First goal is to collect and serve stats for [kornakiewi.cz](https://kornakiewi.cz)
##### Plan:
Road-map:
- ~~Docker package~~
- ~~SSL for dev env~~
- ~~Logging~~
- Model domain for MVP
- Collect basic data when a guest visit page 
- Write JS tracker / tracker generator
- Persist data in DB
- Bot/robots detection
- Simple UI
- Visualisations
- Cache (i.e. Redis, memcached)
- Tests automation
- Messaging queue
##### Architecture goals:
- ###### 12 factor app [5/12]
