# Define base image
FROM microsoft/dotnet:2.2-sdk AS build-env

WORKDIR /source
COPY ["src/App/", "./App/"]

RUN dotnet restore "./App/App.fsproj"

WORKDIR /source/App
RUN dotnet publish -c Release -o /publish

FROM microsoft/dotnet:2.2-aspnetcore-runtime
WORKDIR /publish
COPY --from=build-env /publish .
ENTRYPOINT ["dotnet", "App.dll"]